import random
import math
import datetime 

trajectories = []
trajectories.append([1,4,5])                #0
trajectories.append([0,2,4,5,6])            #1
trajectories.append([1,3,5,6,7])            #2
trajectories.append([2,6,7])                #3
trajectories.append([0,1,5,8,9])            #4
trajectories.append([0,1,2,4,6,8,9,10])     #5
trajectories.append([1,2,3,5,7,9,10,11])    #6
trajectories.append([2,3,6,10,11])          #7
trajectories.append([4,5,9,12,13])          #8
trajectories.append([4,5,6,8,10,12,13,14])  #9
trajectories.append([5,6,7,9,11,13,14,15])  #10
trajectories.append([6,7,10,14,15])         #11
trajectories.append([8,9,13])               #12
trajectories.append([8,9,10,12,14])         #13
trajectories.append([9,10,11,13,15])        #14
trajectories.append([10,11,14])             #15


letters = ['a', 'c', 'd', 'e', 'f', 'g', 'i', 'l', 'm', 'n', 'o', 'r', 't', 'u']

def code_word(word):
    a = []
    for letter in word:
        a.append(letters.index(letter))
    return a

def cw_list(wordlist):
    cw_list = []
    for word in wordlist:
        cw_list.append(code_word(word))
    return cw_list        

class Field():
    def generate(self):        
        self.field = []
        letters_idx = [0, 0, 1, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        for i_row in range(16) :
            idx = math.floor(random.random()*len(letters_idx))
            self.field.append(letters_idx[idx])
            letters_idx.pop(idx)
        
    def check_word(self, coded_word):
        idx = self.letter_idx(coded_word[0])
        for letter in coded_word[1:] :
            idx = self.letter_is_neigbor(letter, idx)            
            if idx == False:
                return False
        return True
            
    def letter_is_neigbor(self, letter, idx):
        for i in trajectories[idx] :
            if letter == self.field[i]:
                return i
        return False
        
    def letter_idx(self, letter):
        return self.field.index(letter)
    
    def __str__(self):
        out = ''
        idx = 1
        for cell in self.field:               
            out = out + ' ' +  str(letters[cell])
            if idx%4 ==0 :
                out = out + '\n'
            idx += 1
        return out
            
   
wordlist = ['guitare', 'gaetan', 'linda', 'amour', 'fete', 'oui']
def check_word():
    n = 0
    start = datetime.datetime.now()
    field = Field()
    init = False
    cw_wordlist = cw_list(wordlist)
    while init == False:
        n=n+1
        field.generate()        
        for cw in cw_wordlist:
            init = field.check_word(cw)
            if init == False:
                break
    print(field)
    print(n)
    print(datetime.datetime.now()-start)
        

check_word()
